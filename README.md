<div align="center">
  <a href="https://husahuc.com/en/projets/ft_ls/">
    <img src="https://gitlab.com/husahuc/ft_ls/raw/master/ft_ls.png" alt="ft_ls">
  </a>
</div>

# Ft_ls
Projet fait pour l'école 42 Lyon. Consistant á refaire la commande ls, avec des options.

# objectif
Commencer avec un petit projet en système qui introduit le filesysteme. Il demande une bonne organisation dès le départ pour faire des options comme -R (récursive, voir les sous-répertoire de dossier).
Voir des cas spéciaux comme le dossier /dev sur unix qui voit des fichier de périphérique. Les liens symboliques. Les droits pour les fichiers.

# Erreurs dans le code
tri non fonctionnel avec multiples options, à refaire avec un quick-sort.

![ft_ls](https://gitlab.com/husahuc/ft_ls/raw/master/ft_ls_1.png)
![ft_ls](https://gitlab.com/husahuc/ft_ls/raw/master/ft_ls_2.png)
![ft_ls](https://gitlab.com/husahuc/ft_ls/raw/master/ft_ls_3.png)