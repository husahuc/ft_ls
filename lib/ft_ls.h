/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_ls.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/24 17:56:47 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 15:04:11 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "../libft/libft.h"
# include <string.h>
# include <errno.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <sys/types.h>
# include <dirent.h>
# include <pwd.h>
# include <grp.h>
# include <time.h>
# include <sys/xattr.h>
# include <sys/acl.h>
# include <sys/ioctl.h>

# define NONE "\033[0m"
# define YELLOW_BLACK "\033[30;43m"
# define RED "\033[31m"
# define GREEN "\033[32m"
# define CYAN "\033[1;36m"
# define BLUE "\033[34m"
# define CYAN_CYAN "\033[34;46m"
# define MAGENTA "\033[35m"
# define DEFAULT_COLOR 1
# define DEFAULT_COLUN 1
# define LEN_LINK 1000

typedef struct	s_settings
{
	int			mg: 1;
	int			mr: 1;
	int			ms: 1;
	int			mt: 1;
	int			md: 1;
	int			mc: 1;
	int			a: 1;
	int			c: 1;
	int			l: 1;
	int			r: 1;
	int			t: 1;
	int			u: 1;
	int			p1: 1;
}				t_settings;

typedef struct	s_dirinfo
{
	char			*name;
	unsigned char	type;
	time_t			time;
	mode_t			mode;
	off_t			size;
	nlink_t			link;
	char			*user;
	char			*group;
	blksize_t		blocks;
	dev_t			dev;
	char			*link_name;
	int				acl: 1;
	int				extended: 1;
}				t_dirinfo;

typedef struct	s_long
{
	int				size;
	int				max_link;
	size_t			max_user;
	size_t			max_group;
	int				max_size;
	int				max_mat;
	int				len;
	int				dev;
	int				max_major;
}				t_long;

/*
** main.c
*/

void			ft_error(char *file);
void			ft_ls(char *path, t_settings *setting, int head, int *ret_ls);
char			*ft_recursive_path(char *path, char *name);
void			ft_ls_recursive(char *path, t_dirinfo **file_dir,
				t_settings *setting, int *ret_ls);
int				main(int ac, char **av);

/*
** get_dir.c
*/

void			ft_not_dir(int ac, char **av, t_settings *setting,
				int nb_dir[3]);
void			ft_error_dir(int ac, char **av, int nb_error);
void			ft_nb_dir(int ac, char **av, int nb_dir[3],
				t_settings *setting);
t_dirinfo		**get_dir_default(t_settings *setting, int info[2]);
t_dirinfo		**get_dir(int ac, char **av, t_settings *setting, int info[2]);

/*
** get_file.c
*/

int				ft_nb_file(char *path, t_settings *setting);
t_dirinfo		**get_file_info(DIR *dir, char *path, t_dirinfo **file_info,
				t_settings *setting);
t_dirinfo		**get_file(char *path, t_settings *setting, int *ret_ls);

/*
** get_param.c
*/

int				ft_error_usage(char ok_setting[11], char c);
t_settings		*ft_put_more_setting(char c, t_settings *setting);
t_settings		*ft_put_setting(char c, t_settings *setting);
int				get_setting(int ac, char **av, t_settings *setting);

/*
** long_format.c
*/

t_long			*init_info_long(t_long *info);
t_long			*get_long_minor_major(t_dirinfo **file, t_long *info, int i);
t_long			*get_info_long(t_dirinfo **file);
void			print_long_file(t_dirinfo *file, t_settings *setting,
				t_long *info);
void			print_long(t_dirinfo **file_dir, t_settings *setting, int opt);

/*
** print_file.c
*/

void			print_color(char *color, char *str);
int				find_type_file(char *name, size_t len_end, char *end);
void			print_color_type(t_dirinfo *dir);
void			print_file(t_dirinfo *dir, t_settings *setting);
void			print_type(t_dirinfo **dir, t_settings *setting);

/*
** printf_long.c
*/

void			print_type_long(t_dirinfo *file);
void			print_other(t_dirinfo *file, t_long *info, t_settings *setting);
void			print_perm_long(t_dirinfo *file);
void			print_major_minor(t_dirinfo *file, t_long *info);
void			print_date_long(t_dirinfo *file, t_settings *setting);

/*
** put_dir.c
*/

unsigned char	get_file_type(mode_t mode);
t_dirinfo		*put_link_name(t_dirinfo *dir, char *path_file);
t_dirinfo		*put_fileinfo_long(t_dirinfo *dir, struct stat stat_dir,
				char *path_file);
t_dirinfo		*put_fileinfo(char *path, char *name,
				t_settings *setting, unsigned char type);
t_dirinfo		*put_dirinfo(char *path, char *name, t_settings *setting);

/*
** sort_function.c
*/

int				ft_sort_size(t_dirinfo *dir1, t_dirinfo *dir2);
int				ft_sort_time(t_dirinfo *dir1, t_dirinfo *dir2);
int				ft_sort_ascii(t_dirinfo *dir1, t_dirinfo *dir2);

/*
** sort.c
*/

int				ft_len_dirinfo(t_dirinfo **dir);
t_dirinfo		**ft_reverse_sort(t_dirinfo **file_dir, int len);
t_dirinfo		**ft_sort(int (*ft)(t_dirinfo *, t_dirinfo *),
				t_dirinfo **file_dir);
t_dirinfo		**sort_dir(t_dirinfo **file_dir, t_settings *setting, int len);

/*
** collun.c
*/

void			print_column(t_dirinfo **file_dir, t_settings *setting);
void			print_column_all(t_dirinfo **file_dir, t_settings *setting,
				int info[3]);
void			print_column_file(t_dirinfo **file_dir, t_settings *setting,
				int j, int info[3]);

#endif
