# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/11/24 16:47:55 by husahuc      #+#   ##    ##    #+#        #
#    Updated: 2019/01/17 11:40:37 by husahuc     ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

CC = gcc
CFLAGS += -Wall -Wextra -Werror
RM = rm -f
NAME = ft_ls
SRC = src/main.c src/get_setting.c src/get_dir.c src/get_file.c src/sort.c \
	src/sort_function.c src/print_file.c src/put_dir.c src/long_format.c \
	src/print_long.c src/column.c
OBJ = $(SRC:.c=.o)
LIB = libft

all: $(NAME)

$(NAME): $(OBJ) $(LIB) lib/ft_ls.h
	@make -C $(LIB)
	@$(CC) -o $(NAME) $(OBJ) $(LIB)/libft.a
	@printf "\n\033[0;32m[OK]\033[0m\033[0;33m Compiling\033[0m %s" $(NAME)

%.o: %.c $(INC)
	@$(CC) -c $(FLAGS) $< -o $@
	@printf "\033[0;32m[OK]\033[0m %s " $<

.PHONY: clean fclean re

clean:
	@make clean -C $(LIB)
	@$(RM) $(DOBJ)$(OBJ)

fclean: clean
	@printf "\033[0;32m[OK]\033[0m \033[0;33msupress:\033[0m %s\n" $(NAME)
	@make fclean -C $(LIB)
	@$(RM) $(NAME)

re: fclean all
