/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   column.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/01/14 13:31:03 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 14:44:00 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

void			print_column_file(t_dirinfo **file_dir, t_settings *setting,
	int j, int info[3])
{
	int		reste;
	int		k;

	print_file(file_dir[j], setting);
	reste = info[0] % info[2] == 0 ? 0 : 1;
	if (j + info[0] / info[2] + reste < info[0])
	{
		k = (info[1] - ft_strlen(file_dir[j]->name)) + 1;
		while (k-- > 0)
			ft_putchar(' ');
	}
	if (!setting->mr)
	{
		free(file_dir[j]->name);
		free(file_dir[j]);
	}
}

void			print_column_all(t_dirinfo **file_dir, t_settings *setting,
				int info[3])
{
	int		i;
	int		j;
	int		reste;

	i = 0;
	while (i * info[2] < info[0])
	{
		j = i;
		while (j < info[0])
		{
			print_column_file(file_dir, setting, j, info);
			reste = info[0] % info[2] == 0 ? 0 : 1;
			j += info[0] / info[2] + reste;
		}
		ft_putchar('\n');
		i++;
	}
}

void			print_column(t_dirinfo **file_dir, t_settings *setting)
{
	int				len_max;
	struct winsize	win;
	int				nb_element;
	int				elem_size;
	int				info[3];

	len_max = 0;
	nb_element = 0;
	while (file_dir[nb_element] != NULL)
	{
		if (ft_strlen(file_dir[nb_element]->name) > len_max)
			len_max = ft_strlen(file_dir[nb_element]->name);
		nb_element++;
	}
	ioctl(0, TIOCGWINSZ, &win);
	if (win.ws_col <= len_max)
	{
		setting->mc = 0;
		print_type(file_dir, setting);
		return ;
	}
	info[0] = nb_element;
	info[1] = len_max;
	info[2] = win.ws_col / (len_max + 1);
	print_column_all(file_dir, setting, info);
}
