/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/28 18:00:23 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/16 14:26:47 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

int			ft_len_dirinfo(t_dirinfo **dir)
{
	int i;

	i = 0;
	while (dir[i] != NULL)
		i++;
	return (i);
}

t_dirinfo	**ft_reverse_sort(t_dirinfo **file_dir, int len)
{
	int			i;
	int			j;
	t_dirinfo	*buf;

	i = 0;
	j = len - 1;
	while (i < (len / 2))
	{
		buf = file_dir[i];
		file_dir[i] = file_dir[j];
		file_dir[j] = buf;
		i++;
		j--;
	}
	return (file_dir);
}

t_dirinfo	**ft_sort(int (*ft)(t_dirinfo *, t_dirinfo *), t_dirinfo **file_dir)
{
	int			i;
	int			j;
	t_dirinfo	*buf;

	i = 0;
	while (file_dir[i] != NULL)
	{
		j = i + 1;
		while (file_dir[j] != NULL)
		{
			if (ft(file_dir[i], file_dir[j]) == 1)
			{
				buf = file_dir[i];
				file_dir[i] = file_dir[j];
				file_dir[j] = buf;
			}
			j++;
		}
		i++;
	}
	return (file_dir);
}

t_dirinfo	**sort_dir(t_dirinfo **file_dir, t_settings *setting, int len)
{
	ft_sort(&ft_sort_ascii, file_dir);
	if (setting->ms == -1)
		ft_sort(&ft_sort_size, file_dir);
	else if (setting->t == -1)
		ft_sort(&ft_sort_time, file_dir);
	if (setting->r == -1)
		ft_reverse_sort(file_dir, len);
	return (file_dir);
}
