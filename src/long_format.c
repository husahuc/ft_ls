/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   long_format.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/06 18:41:42 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 13:23:11 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

t_long	*init_info_long(t_long *info)
{
	info->size = 0;
	info->max_link = 0;
	info->max_user = 0;
	info->max_group = 0;
	info->max_size = 0;
	info->max_mat = 0;
	info->max_major = 0;
	info->dev = 0;
	return (info);
}

t_long	*get_long_minor_major(t_dirinfo **file, t_long *info, int i)
{
	if (ft_count_len((int32_t)(((file[i]->dev) >> 24) & 0xff))
		> info->max_major)
		info->max_major =
			ft_count_len((int32_t)(((file[i]->dev) >> 24) & 0xff));
	info->dev = 1;
	return (info);
}

t_long	*get_info_long(t_dirinfo **file)
{
	int		i;
	t_long	*info;

	if (!(info = malloc(sizeof(t_long))))
		return (NULL);
	info = init_info_long(info);
	i = -1;
	while (file[++i] != NULL)
	{
		info->size += file[i]->blocks;
		if (ft_count_len(file[i]->link) > info->max_link)
			info->max_link = ft_count_len(file[i]->link);
		if (ft_strlen(file[i]->user) > info->max_user)
			info->max_user = ft_strlen(file[i]->user);
		if (ft_strlen(file[i]->group) > info->max_group)
			info->max_group = ft_strlen(file[i]->group);
		if (ft_count_len(file[i]->size) > info->max_size)
			info->max_size = ft_count_len(file[i]->size);
		if (S_ISCHR(file[i]->mode) || S_ISBLK(file[i]->mode))
			get_long_minor_major(file, info, i);
	}
	info->len = i;
	return (info);
}

void	print_long_file(t_dirinfo *file, t_settings *setting, t_long *info)
{
	print_type_long(file);
	print_perm_long(file);
	if (file->extended == -1)
		ft_putchar('@');
	else if (file->acl == -1)
		ft_putchar('+');
	else
		ft_putchar(' ');
	print_other(file, info, setting);
	print_file(file, setting);
	if (S_ISLNK(file->mode))
	{
		ft_putstr(" -> ");
		ft_putstr(file->link_name);
		free(file->link_name);
	}
	ft_putchar('\n');
}

void	print_long(t_dirinfo **file_dir, t_settings *setting, int opt)
{
	t_long	*info;
	int		i;

	if ((info = get_info_long(file_dir)) == NULL)
		return ;
	if (!opt && info->len > 0)
	{
		ft_putstr("total ");
		ft_putnbr(info->size);
		ft_putchar('\n');
	}
	i = 0;
	while (file_dir[i] != NULL)
	{
		print_long_file(file_dir[i], setting, info);
		if (!setting->mr)
		{
			free(file_dir[i]->name);
			free(file_dir[i]);
		}
		i++;
	}
	free(info);
}
