/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   put_dir.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/05 19:08:11 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 11:22:04 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

unsigned char	get_file_type(mode_t mode)
{
	if (((mode) & S_IFMT) == S_IFBLK)
		return (DT_BLK);
	if (((mode) & S_IFMT) == S_IFREG)
		return (DT_REG);
	if (((mode) & S_IFMT) == S_IFLNK)
		return (DT_LNK);
	if (((mode) & S_IFMT) == S_IFDIR)
		return (DT_DIR);
	if (((mode) & S_IFMT) == S_IFCHR)
		return (DT_CHR);
	if (((mode) & S_IFMT) == S_IFIFO)
		return (DT_FIFO);
	if (((mode) & S_IFMT) == S_IFSOCK)
		return (DT_SOCK);
	return (0);
}

t_dirinfo		*put_link_name(t_dirinfo *dir, char *path_file)
{
	ssize_t		len;
	char		link_buf[LEN_LINK];

	len = readlink(path_file, link_buf, LEN_LINK);
	link_buf[len] = '\0';
	dir->link_name = ft_strdup(link_buf);
	return (dir);
}

t_dirinfo		*put_fileinfo_long(t_dirinfo *dir, struct stat stat_dir,
	char *path_file)
{
	struct group	*group;
	struct passwd	*user;
	acl_t			acl;
	char			l[1024];
	ssize_t			extended;

	dir->link = stat_dir.st_nlink;
	if ((user = getpwuid(stat_dir.st_uid)))
		dir->user = user->pw_name;
	group = getgrgid(stat_dir.st_gid);
	dir->group = group->gr_name;
	dir->blocks = stat_dir.st_blocks;
	dir->dev = stat_dir.st_rdev;
	dir->acl =
		(acl = acl_get_file(path_file, ACL_TYPE_EXTENDED)) ? -1 : 0;
	dir->extended =
		(extended = listxattr(path_file, l, 1020, XATTR_NOFOLLOW)) > 0
		? -1 : 0;
	acl_free((void *)acl);
	return (dir);
}

t_dirinfo		*put_fileinfo(char *path, char *name,
	t_settings *setting, unsigned char type)
{
	t_dirinfo		*dir;
	struct stat		stat_dir;
	char			*path_file;

	lstat(path_file = ft_strjoin(path, name), &stat_dir);
	if (!(dir = malloc(sizeof(t_dirinfo)))
		|| !(dir->name = ft_strdup(name)))
		return (NULL);
	type == DT_UNKNOWN ? (dir->type = get_file_type(stat_dir.st_mode)) :
		(dir->type = type);
	if (dir->type == DT_LNK && setting->l == -1)
		dir = put_link_name(dir, path_file);
	setting->u == -1 ? (dir->time = stat_dir.st_atime) :
		(dir->time = stat_dir.st_mtime);
	dir->mode = stat_dir.st_mode;
	dir->size = stat_dir.st_size;
	if (setting->l == -1)
		dir = put_fileinfo_long(dir, stat_dir, path_file);
	free(path_file);
	return (dir);
}

t_dirinfo		*put_dirinfo(char *path, char *name, t_settings *setting)
{
	t_dirinfo		*dir;
	struct stat		stat_dir;
	struct passwd	*user;
	struct group	*group;
	char			*path_file;

	lstat(path_file = ft_strjoin(path, name), &stat_dir);
	free(path_file);
	if (!(dir = malloc(sizeof(t_dirinfo)))
		|| !(dir->name = ft_strdup(name)))
		return (NULL);
	if (setting->u)
		dir->time = stat_dir.st_atime;
	else
		dir->time = stat_dir.st_mtime;
	dir->size = stat_dir.st_size;
	return (dir);
}
