/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/24 16:59:49 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/14 12:31:14 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

void	ft_error(char *file)
{
	ft_putstr_fd("ls: ", 2);
	perror(file);
}

void	ft_ls(char *path, t_settings *setting, int head, int *ret_ls)
{
	t_dirinfo	**file_dir;

	if (head == 1)
	{
		ft_putstr(path);
		ft_putstr(":\n");
	}
	if ((file_dir = get_file(path, setting, ret_ls)) == NULL)
		return ;
	print_type(file_dir, setting);
	if (setting->mr == -1)
		ft_ls_recursive(path, file_dir, setting, ret_ls);
	free(file_dir);
}

char	*ft_recursive_path(char *path, char *name)
{
	char *str;
	char *correct_path;

	if (path[ft_strlen(path) - 1] != '/')
		correct_path = ft_strjoin(path, "/");
	else
		correct_path = ft_strdup(path);
	str = ft_strjoin(correct_path, name);
	free(correct_path);
	return (str);
}

void	ft_ls_recursive(char *path, t_dirinfo **file_dir,
	t_settings *setting, int *ret_ls)
{
	int		j;
	char	*buf;

	j = 0;
	while (file_dir[j] != NULL)
	{
		if (file_dir[j]->type == DT_DIR &&
			ft_strcmp("..", file_dir[j]->name) != 0 &&
			ft_strcmp(".", file_dir[j]->name) != 0)
		{
			ft_putchar('\n');
			ft_ls(buf = ft_recursive_path(path, file_dir[j]->name)
			, setting, 1, ret_ls);
			free(buf);
		}
		free(file_dir[j]->name);
		free(file_dir[j]);
		j++;
	}
}

int		main(int ac, char **av)
{
	t_dirinfo	**ls_dir;
	t_settings	*setting;
	int			i;
	int			info[2];
	int			ret_ls;

	setting = malloc(sizeof(t_settings));
	if (((i = get_setting(ac, av, setting)) == 0)
		|| !(ls_dir = get_dir(i, av, setting, info)))
		return (1);
	i = 0;
	ret_ls = 0;
	while (ls_dir[i])
	{
		ft_ls(ls_dir[i]->name, setting, info[1], &ret_ls);
		if (i != info[0] - 1)
			ft_putchar('\n');
		free(ls_dir[i]->name);
		free(ls_dir[i++]);
	}
	free(ls_dir);
	free(setting);
	return (ret_ls);
}
