/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_file.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/28 19:32:45 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/14 12:28:34 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

int			ft_nb_file(char *path, t_settings *setting)
{
	int				i;
	struct dirent	*file;
	DIR				*dir;

	if ((dir = opendir(path)) == NULL)
		return (0);
	i = 0;
	while ((file = readdir(dir)))
		if (file->d_name[0] != '.' || setting->a == 1)
			i++;
	closedir(dir);
	return (i);
}

t_dirinfo	**get_file_info(DIR *dir, char *path, t_dirinfo **file_info,
	t_settings *setting)
{
	int				i;
	struct dirent	*file;
	char			*new_path;

	i = 0;
	while ((file = readdir(dir)))
	{
		if (file->d_name[0] != '.' || setting->a == -1)
		{
			if (path[ft_strlen(path) - 1] != '/')
			{
				new_path = ft_strjoin(path, "/");
				file_info[i++] =
					put_fileinfo(new_path, file->d_name, setting, file->d_type);
				free(new_path);
			}
			else
				file_info[i++] =
					put_fileinfo(path, file->d_name, setting, file->d_type);
		}
	}
	file_info[i] = NULL;
	sort_dir(file_info, setting, i);
	return (file_info);
}

t_dirinfo	**get_file(char *path, t_settings *setting, int *ret_ls)
{
	t_dirinfo		**file_info;
	DIR				*dir;

	file_info = malloc(sizeof(t_dirinfo) * (ft_nb_file(path, setting) + 1));
	if ((dir = opendir(path)) == NULL)
	{
		*ret_ls = 1;
		ft_error(path);
		return (NULL);
	}
	get_file_info(dir, path, file_info, setting);
	closedir(dir);
	return (file_info);
}
