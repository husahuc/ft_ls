/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   sort_function.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/05 11:36:07 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/13 17:44:39 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

int		ft_sort_size(t_dirinfo *dir1, t_dirinfo *dir2)
{
	if (dir1->size < dir2->size)
		return (1);
	return (0);
}

int		ft_sort_time(t_dirinfo *dir1, t_dirinfo *dir2)
{
	if (dir1->time < dir2->time)
		return (1);
	return (0);
}

int		ft_sort_ascii(t_dirinfo *dir1, t_dirinfo *dir2)
{
	if (ft_strcmp(dir1->name, dir2->name) > 0)
		return (1);
	return (0);
}
