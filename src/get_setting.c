/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_setting.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/25 13:52:58 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 11:29:41 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

int			ft_error_usage(char ok_setting[11], char c)
{
	ft_putstr_fd("ls: illegal option -- ", 2);
	ft_putchar_fd(c, 2);
	ft_putstr_fd("\nusage: ls [-", 2);
	ft_putstr_fd(ok_setting, 2);
	ft_putendl_fd("] [file ...]", 2);
	return (0);
}

t_settings	*ft_put_more_setting(char c, t_settings *setting)
{
	if (c == 'G')
	{
		setting->md = 0;
		setting->mg = 1;
	}
	if (c == 'R')
		setting->mr = 1;
	if (c == 'S')
		setting->ms = 1;
	if (c == 'T')
		setting->mt = 1;
	if (c == 'D')
	{
		setting->mg = 0;
		setting->md = 1;
	}
	if (c == 'C')
	{
		setting->l = 0;
		setting->p1 = 0;
		setting->mc = 1;
	}
	return (setting);
}

t_settings	*ft_put_setting(char c, t_settings *setting)
{
	setting = ft_put_more_setting(c, setting);
	if (c == 'a')
		setting->a = 1;
	if (c == 'c')
		setting->c = 1;
	if (c == 'l')
	{
		setting->mc = 0;
		setting->p1 = 0;
		setting->l = 1;
	}
	if (c == 'r')
		setting->r = 1;
	if (c == 't')
		setting->t = 1;
	if (c == 'u')
		setting->u = 1;
	if (c == '1')
	{
		setting->mc = 0;
		setting->l = 0;
		setting->p1 = 1;
	}
	return (setting);
}

t_settings	*get_default(t_settings *setting)
{
	setting->mg = DEFAULT_COLOR;
	setting->mr = 0;
	setting->ms = 0;
	setting->mt = 0;
	setting->md = 0;
	setting->mc = DEFAULT_COLUN;
	setting->a = 0;
	setting->c = 0;
	setting->l = 0;
	setting->r = 0;
	setting->t = 0;
	setting->u = 0;
	setting->p1 = 0;
	return (setting);
}

int			get_setting(int ac, char **av, t_settings *setting)
{
	char		ok_setting[14];
	int			i;
	int			j;

	ft_strcpy(ok_setting, "CDGRSTaclrtu1");
	setting = get_default(setting);
	i = 1;
	while (i < ac && ft_strlen(av[i]) >= 2 && av[i][0] == '-')
	{
		j = 0;
		if (ft_strcmp("--", av[i]) == 0)
			return (++i);
		while (av[i][++j] != 0)
		{
			if (ft_strchr(ok_setting, av[i][j]))
				setting = ft_put_setting(av[i][j], setting);
			else
			{
				free(setting);
				return (ft_error_usage(ok_setting, av[i][j++]));
			}
		}
		i++;
	}
	return (i);
}
