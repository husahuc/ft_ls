/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_dir.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/26 11:20:44 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 14:27:51 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

void		ft_not_dir(int ac, char **av, t_settings *setting, int nb_dir[3])
{
	struct stat stat_dir;
	t_dirinfo	**file_info;
	int			i;

	file_info = malloc(sizeof(t_dirinfo) * (nb_dir[2] + 1));
	i = 0;
	while (av[ac])
	{
		if (lstat(av[ac], &stat_dir) >= 0
			&& !((stat_dir.st_mode & S_IFMT) == S_IFDIR))
			file_info[i++] = put_fileinfo("./", av[ac], setting, 0);
		ac++;
	}
	file_info[i] = NULL;
	sort_dir(file_info, setting, i);
	if (setting->l == -1)
		print_long(file_info, setting, 1);
	else
		print_type(file_info, setting);
	if (nb_dir[0] > 0)
		ft_putchar('\n');
	free(file_info);
}

void		ft_error_dir(int ac, char **av, int nb_error)
{
	struct stat	stat_dir;
	char		**error;
	int			i;
	int			j;

	error = malloc(sizeof(char*) * nb_error);
	i = 0;
	while (av[ac])
	{
		if (lstat(av[ac], &stat_dir) == -1)
		{
			error[i] = ft_strdup(av[ac]);
			i++;
		}
		ac++;
	}
	sort_list_world(&ft_strcmp, error, nb_error);
	j = 0;
	while (j < i)
	{
		ft_error(error[j]);
		free(error[j++]);
	}
	free(error);
}

void		ft_nb_dir(int ac, char **av, int nb_dir[3], t_settings *setting)
{
	struct stat	stat_dir;
	int			buf;

	nb_dir[0] = 0;
	nb_dir[1] = 0;
	nb_dir[2] = 0;
	buf = ac;
	while (av[ac])
	{
		if (stat(av[ac], &stat_dir) == -1 && lstat(av[ac], &stat_dir) == -1)
			nb_dir[1]++;
		else if (!((stat_dir.st_mode & S_IFMT) == S_IFDIR))
			nb_dir[2]++;
		else
			nb_dir[0]++;
		ac++;
	}
	if (nb_dir[1] > 0)
		ft_error_dir(buf, av, nb_dir[1]);
	if (nb_dir[2] > 0)
		ft_not_dir(buf, av, setting, nb_dir);
}

t_dirinfo	**get_dir_default(t_settings *setting, int info[2])
{
	t_dirinfo	**ret_dir;

	ret_dir = malloc(sizeof(t_dirinfo) * 2);
	ret_dir[0] = put_dirinfo("./", ".", setting);
	ret_dir[1] = NULL;
	info[0] = 1;
	return (ret_dir);
}

t_dirinfo	**get_dir(int ac, char **av, t_settings *setting,
	int info[2])
{
	struct stat	stat_dir;
	t_dirinfo	**ret_dir;
	int			nb_dir[3];
	int			i;

	ft_nb_dir(ac, av, nb_dir, setting);
	info[1] = 0;
	if (nb_dir[1] == 0 && nb_dir[2] == 0 && nb_dir[0] == 0)
		return (get_dir_default(setting, info));
	if (!(ret_dir = malloc(sizeof(t_dirinfo) * nb_dir[0] + 1)))
		return (NULL);
	info[0] = nb_dir[0];
	i = 0;
	while (av[ac])
	{
		if (stat(av[ac], &stat_dir) >= 0 && stat_dir.st_mode & S_IFDIR)
			if (!(ret_dir[i++] = put_dirinfo("./", av[ac], setting)))
				return (NULL);
		ac++;
	}
	ret_dir[i] = NULL;
	if (nb_dir[0] > 1 || nb_dir[1] > 0)
		info[1] = 1;
	sort_dir(ret_dir, setting, i);
	return (ret_dir);
}
