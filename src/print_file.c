/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_file.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/28 11:12:27 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 14:45:25 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

void		print_color(char *color, char *str)
{
	ft_putstr(color);
	ft_putstr(str);
	ft_putstr(NONE);
}

int			find_type_file(char *name, size_t len_end, char *end)
{
	char	*buf;

	if (ft_strlen(name) > len_end)
	{
		buf = ft_strsub(name, (ft_strlen(name) - len_end), len_end);
		if (!ft_strcmp(buf, end))
		{
			free(buf);
			return (1);
		}
		free(buf);
		return (0);
	}
	return (0);
}

void		print_color_type(t_dirinfo *dir)
{
	if (!ft_strcmp(dir->name, "Makefile") || !ft_strcmp(dir->name, "makefile"))
		print_color(CYAN, dir->name);
	else if (find_type_file(dir->name, 2, ".c"))
		print_color(GREEN, dir->name);
	else if (find_type_file(dir->name, 2, ".o"))
		print_color(MAGENTA, dir->name);
	else if (find_type_file(dir->name, 2, ".h"))
		print_color(BLUE, dir->name);
	else
		ft_putstr(dir->name);
}

void		print_file(t_dirinfo *dir, t_settings *setting)
{
	if (setting->md && setting->mg)
		return (ft_putstr(dir->name));
	if (setting->md == -1)
		return (print_color_type(dir));
	else if (setting->mg == -1 && dir->type == DT_DIR)
		return (print_color(CYAN, dir->name));
	else if (setting->mg == -1 && dir->type == DT_LNK)
		return (print_color(MAGENTA, dir->name));
	else if (setting->mg == -1 && dir->type == DT_BLK)
		return (print_color(CYAN_CYAN, dir->name));
	else if (setting->mg == -1 && dir->type == DT_CHR)
		return (print_color(YELLOW_BLACK, dir->name));
	else if (setting->mg == -1 && dir->type == DT_SOCK)
		return (print_color(GREEN, dir->name));
	else if (setting->mg == -1 && dir->mode & S_IXUSR)
		return (print_color(RED, dir->name));
	return (ft_putstr(dir->name));
}

void		print_type(t_dirinfo **dir, t_settings *setting)
{
	int i;

	if (setting->mc == -1)
		print_column(dir, setting);
	else if (setting->l == -1)
		print_long(dir, setting, 0);
	else
	{
		i = 0;
		while (dir[i] != NULL)
		{
			print_file(dir[i], setting);
			ft_putchar('\n');
			if (!setting->mr)
			{
				free(dir[i]->name);
				free(dir[i]);
			}
			i++;
		}
	}
}
