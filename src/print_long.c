/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_long.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/31 14:17:53 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/01/17 13:20:18 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/ft_ls.h"

void	print_type_long(t_dirinfo *file)
{
	if (file->type == DT_LNK)
		ft_putchar('l');
	else if (file->type == DT_SOCK)
		ft_putchar('s');
	else if (file->type == DT_DIR)
		ft_putchar('d');
	else if (file->type == DT_REG)
		ft_putchar('-');
	else if (file->type == DT_CHR)
		ft_putchar('c');
	else if (file->type == DT_BLK)
		ft_putchar('b');
	else if (file->type == DT_FIFO)
		ft_putchar('p');
}

void	print_other(t_dirinfo *file, t_long *info, t_settings *setting)
{
	int		i;

	i = (info->max_link - ft_count_len(file->link)) + 1;
	while (i-- > 0)
		ft_putchar(' ');
	ft_putnbr(file->link);
	ft_putchar(' ');
	ft_putstr(file->user);
	i = (info->max_user - ft_strlen(file->user)) + 2;
	while (i-- > 0)
		ft_putchar(' ');
	ft_putstr(file->group);
	i = (info->max_group - ft_strlen(file->group)) + 2;
	while (i-- > 0)
		ft_putchar(' ');
	i = (info->max_size - ft_count_len(file->size));
	while (i-- > 0)
		ft_putchar(' ');
	if (info->dev == 1)
		print_major_minor(file, info);
	else
		ft_putnbr(file->size);
	ft_putchar(' ');
	print_date_long(file, setting);
	ft_putchar(' ');
}

void	print_perm_long(t_dirinfo *file)
{
	ft_putchar(file->mode & S_IRUSR ? 'r' : '-');
	ft_putchar(file->mode & S_IWUSR ? 'w' : '-');
	if ((file->mode & S_ISUID))
		ft_putchar(!(file->mode & S_IXUSR) ? 'S' : 's');
	else
		ft_putchar(file->mode & S_IXUSR ? 'x' : '-');
	ft_putchar(file->mode & S_IRGRP ? 'r' : '-');
	ft_putchar(file->mode & S_IWGRP ? 'w' : '-');
	if (file->mode & S_ISGID)
		ft_putchar(!(file->mode & S_IXGRP) ? 'S' : 's');
	else
		ft_putchar(file->mode & S_IXGRP ? 'x' : '-');
	ft_putchar(file->mode & S_IROTH ? 'r' : '-');
	ft_putchar(file->mode & S_IWOTH ? 'w' : '-');
	if (file->mode & S_ISVTX)
		ft_putchar(!(file->mode & S_IXOTH) ? 'T' : 't');
	else
		ft_putchar(file->mode & S_IXOTH ? 'x' : '-');
}

void	print_major_minor(t_dirinfo *file, t_long *info)
{
	int i;

	i = info->max_major -
		ft_count_len((int32_t)(((file->dev) >> 24) & 0xff)) + 1;
	while (i-- > 0)
		ft_putchar(' ');
	if (file->type == DT_SOCK || file->type == DT_CHR || file->type == DT_BLK)
	{
		ft_putnbr((int32_t)(((file->dev) >> 24) & 0xff));
		ft_putchar(',');
	}
	i = 3 - ft_count_len(((int32_t)((file->dev) & 0xffffff))) + 1;
	while (i-- > 0)
		ft_putchar(' ');
	if (file->type == DT_SOCK || file->type == DT_CHR || file->type == DT_BLK)
		ft_putnbr(((int32_t)((file->dev) & 0xffffff)));
	else
	{
		ft_putstr("  ");
		ft_putnbr(file->size);
	}
}

void	print_date_long(t_dirinfo *file, t_settings *setting)
{
	time_t	present;
	char	*char_time;

	time(&present);
	if (setting->mt == -1)
		ft_putstr(char_time = ft_strsub(ctime(&file->time), 4, 20));
	else if (file->time <= present - 15768000
		|| file->time > present)
	{
		ft_putstr(char_time = ft_strsub(ctime(&file->time), 4, 7));
		free(char_time);
		if (file->time <= present - 15768000)
			ft_putstr(char_time = ft_strsub(ctime(&file->time), 19, 5));
		if (file->time > present)
		{
			if (file->time >= 253402297200)
				ft_putstr(char_time = ft_strsub(ctime(&file->time), 23, 6));
			else
				ft_putstr(char_time = ft_strsub(ctime(&file->time), 19, 5));
		}
	}
	else
		ft_putstr(char_time = ft_strsub(ctime(&file->time), 4, 12));
	free(char_time);
}
