/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_itoa.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 16:22:20 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2018/12/09 16:21:35 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char		*ft_itoa(int n)
{
	char	*new_nb;
	int		i;
	int		ng;

	ng = 0;
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	if (n == 0)
		return (ft_strdup("0"));
	if (!(new_nb = ft_strnew(ft_count_len(n))))
		return (NULL);
	i = ft_count_len(n) - 1;
	if (n < 0)
	{
		new_nb[0] = '-';
		n = -n;
		ng = 1;
	}
	while (i >= ng)
	{
		new_nb[i--] = (n % 10) + '0';
		n = n / 10;
	}
	return (new_nb);
}
