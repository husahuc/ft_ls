/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   get_next_line.h                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/08 18:22:48 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2018/10/22 08:51:06 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>

# define BUFF_SIZE 1

typedef struct			s_gnl_list
{
	char				*str;
	int					fd;
	struct s_gnl_list	*next;
}						t_gnl_list;

int						get_next_line(const int fd, char **line);

#endif
