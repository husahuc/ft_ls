/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strcchr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/25 15:21:12 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2018/11/25 15:40:42 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	ft_strcchr(const char *str, int c)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == (const char)c)
			return (str[i]);
		i++;
	}
	if (str[i] == (char)c)
		return (str[i]);
	return ('\0');
}
