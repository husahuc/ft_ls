/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_count_len.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/12/09 16:17:54 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2018/12/09 17:26:23 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int		ft_count_len(int nb)
{
	int len;

	len = 0;
	if (nb == 0)
		return (1);
	if (nb > 0)
	{
		while (nb > 0)
		{
			len++;
			nb = nb / 10;
		}
	}
	else
		return (ft_count_len(-nb) + 1);
	return (len);
}
